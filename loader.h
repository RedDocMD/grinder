#ifndef __LOADER_H__
#define __LOADER_H__

#include <stddef.h>
#include <GL/glew.h>
#include <GL/gl.h>

enum ShaderKind {
	SK_Vertex,
	SK_Fragment,
};

GLuint load_shader_from_disk(enum ShaderKind kind, const char *filename,
			  GLuint program_id);
GLuint load_shader_from_text(enum ShaderKind kind, const char *content,
			  GLuint program_id);

#endif // __LOADER_H__

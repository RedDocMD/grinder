#ifndef __VECTOR_H__
#define __VECTOR_H__

#include <stdint.h>
#include <stddef.h>

// Resizable generic vector
// NOT small size optimized
struct vector {
	uint8_t *buf;
	size_t len;
	size_t cap;
	size_t el_sz;
	void (*el_dest_func)(void *);
};

void vector_init(struct vector *vec, size_t el_sz,
		 void (*el_dest_func)(void *));
void vector_free(struct vector *vec);

size_t vector_len(struct vector *vec);

// Pointer to single element of size same as vec->el_sz
// Element copied into end of vector
// Return 0 on success or error code on failure.
int vector_push(struct vector *vec, const void *el);

// Remove last element of vector
void vector_pop(struct vector *vec);

// Copies idx'th element of vec into el.
// Return 0 on success or error code on failure
int vector_at(const struct vector *vec, size_t idx, void *el);

// Returns pointer to idx'th element of vec
// Returns NULL on failure.
const void *vector_at_ref(const struct vector *vec, size_t idx);
void *vector_at_ref_mut(struct vector *vec, size_t idx);

// Error Codes
#define VEC_NO_MEM -1
#define VEC_OUT_OF_BOUNDS -2

#endif // __VECTOR_H__

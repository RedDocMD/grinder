#include "vector.h"
#include <string.h>
#include <stdlib.h>

void vector_init(struct vector *vec, size_t el_sz, void (*el_dest_func)(void *))
{
	memset(vec, 0, sizeof(*vec));
	vec->el_sz = el_sz;
	vec->el_dest_func = el_dest_func;
}

void vector_free(struct vector *vec)
{
	if (vec->el_dest_func) {
		uint8_t *ptr;
		size_t i;

		i = 0;
		ptr = vec->buf;
		while (i < vec->len) {
			(*vec->el_dest_func)(ptr);
			++i;
			ptr += vec->el_sz;
		}
	}
	free(vec->buf);
}

size_t vector_len(struct vector *vec)
{
	return vec->len;
}

static int vector_resize(struct vector *vec, size_t new_cap)
{
	size_t new_sz = new_cap * vec->el_sz;
	void *ptr = realloc(vec->buf, new_sz);
	if (ptr) {
		vec->buf = ptr;
		vec->cap = new_cap;
		return 0;
	}
	return VEC_NO_MEM;
}

#define VECTOR_IDX(vec, idx) ((vec)->buf + (vec)->el_sz * (idx))

int vector_push(struct vector *vec, const void *el)
{
	int ret;
	if (vec->cap == 0) {
		if ((ret = vector_resize(vec, 8)) != 0)
			return ret;
	} else if (vec->cap == vec->len) {
		if ((ret = vector_resize(vec, vec->cap * 3 / 2)) != 0)
			return ret;
	}
	memcpy(VECTOR_IDX(vec, vec->len), el, vec->el_sz);
	++(vec->len);
	return 0;
}

int vector_at(const struct vector *vec, size_t idx, void *el)
{
	if (idx >= vec->len)
		return VEC_OUT_OF_BOUNDS;
	memcpy(el, VECTOR_IDX(vec, idx), vec->el_sz);
	return 0;
}

const void *vector_at_ref(const struct vector *vec, size_t idx)
{
	if (idx >= vec->len)
		return NULL;
	return VECTOR_IDX(vec, idx);
}

void *vector_at_ref_mut(struct vector *vec, size_t idx)
{
	if (idx >= vec->len)
		return NULL;
	return VECTOR_IDX(vec, idx);
}

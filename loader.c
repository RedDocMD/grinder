#include "loader.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>

GLuint load_shader_from_disk(enum ShaderKind kind, const char *filename,
			     GLuint program_id)
{
	GArray *content;
	char buf[1024];
	FILE *fptr;
	GLuint shader_id;

	if ((fptr = fopen(filename, "r")) == NULL) {
		fprintf(stderr, "Failed to open %s for reading\n", filename);
		exit(1);
	}

	content = g_array_new(true, false, 1);
	for (;;) {
		if (fgets(buf, sizeof(buf), fptr)) {
			g_array_append_vals(content, buf, strlen(buf));
		} else {
			if (ferror(fptr)) {
				fprintf(stderr, "Failure while reading %s\n",
					filename);
				exit(1);
			}
			if (feof(fptr))
				break;
		}
	}

	shader_id = load_shader_from_text(kind, content->data, program_id);

	fclose(fptr);
	g_array_unref(content);

	return shader_id;
}

GLuint load_shader_from_text(enum ShaderKind kind, const char *content,
			     GLuint program_id)
{
	GLuint shader_id;
	GLint result, info_log_len;
	GLchar *mess;

	switch (kind) {
	case SK_Vertex:
		shader_id = glCreateShader(GL_VERTEX_SHADER);
		break;
	case SK_Fragment:
		shader_id = glCreateShader(GL_FRAGMENT_SHADER);
		break;
	default:
		fprintf(stderr, "Invalid shader kind");
		exit(1);
	}

	glShaderSource(shader_id, 1, &content, NULL);
	glCompileShader(shader_id);

	glGetShaderiv(shader_id, GL_COMPILE_STATUS, &result);
	glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_log_len);

	if (info_log_len > 0) {
		mess = malloc(info_log_len + 1);
		glGetShaderInfoLog(shader_id, info_log_len, NULL, mess);
		fprintf(stderr, "%s\n", mess);
		free(mess);
	}
	if (result == GL_FALSE) {
		fprintf(stderr, "Failed to compile shader\n");
		exit(1);
	}

	glAttachShader(program_id, shader_id);

	return shader_id;
}

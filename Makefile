CC := clang
CFLAGS := -Wall -std=c11

# GTK-related flags
CFLAGS += $(shell pkg-config --cflags gtk4)
LDFLAGS += $(shell pkg-config --libs gtk4)

# OpenGL related flags
CFLAGS += $(shell pkg-config --cflags glew)
LDFLAGS += $(shell pkg-config --libs glew)

ifeq ($(DEBUG), 1)
	CFLAGS += -g
endif

ifeq ($(ASAN), 1)
	CFLAGS += -fsanitize=address -fsanitize=undefined -fno-omit-frame-pointer
endif

ifeq ($(RELEASE), 1)
	CFLAGS += -O2
endif

SOURCES := $(wildcard *.c)
OBJECTS := $(patsubst %.c,%.o,$(SOURCES))

MAIN_FILES := test_vector.c basic_gl.c
MAIN_OBJECTS := $(patsubst %.c,%.o,$(MAIN_FILES))
NON_MAIN_OBJECTS := $(filter-out $(MAIN_OBJECTS),$(OBJECTS))

all: $(OBJECTS)

test_vector: test_vector.o $(NON_MAIN_OBJECTS)
	$(CC) $(CFLAGS) -o bin/$@ $^ $(LDFLAGS)

basic_gl: basic_gl.o $(NON_MAIN_OBJECTS)
	$(CC) $(CFLAGS) -o bin/$@ $^ $(LDFLAGS)

clean:
	rm -f *.o
	rm -rf bin/
	mkdir bin

.PHONY: all clean

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <linux/limits.h>
#include <sys/stat.h>

#include "vector.h"

struct stat_data {
	char *filename;
	struct stat data;
};

void make_stat_data(const char *name, const struct stat *statbuf,
		    struct stat_data *data)
{
	data->filename = malloc(strlen(name) + 1);
	strcpy(data->filename, name);
	memcpy(&data->data, statbuf, sizeof(*statbuf));
}

void free_stat_data(void *ptr)
{
	struct stat_data *data = ptr;
	free(data->filename);
}

void sort_files(struct vector *filevec)
{
	struct stat_data *data1, *data2;
	struct stat_data tmp;
	size_t i, j, len;

	len = vector_len(filevec);
	for (i = 0; i < len; i++) {
		for (j = i + 1; j < len; j++) {
			data1 = vector_at_ref_mut(filevec, i);
			data2 = vector_at_ref_mut(filevec, j);
			if (strcmp(data1->filename, data2->filename) > 0) {
				tmp = *data1;
				*data1 = *data2;
				*data2 = tmp;
			}
		}
	}
}

int main(int argc, char *argv[])
{
	char *basedir, *entry_name;
	size_t basedir_name_len, entry_name_cap, i;
	DIR *dirp;
	struct dirent *entry;
	struct stat statbuf;
	struct stat_data data;
	struct vector statvec;

	basedir_name_len = strlen(argv[1]);
	basedir = malloc(basedir_name_len + 1);
	strncpy(basedir, argv[1], basedir_name_len + 1);

	// Extra char for forward slash
	entry_name_cap = basedir_name_len + NAME_MAX + 1;
	entry_name = malloc(entry_name_cap + 1);
	memset(entry_name, 0, entry_name_cap);
	strncpy(entry_name, basedir, basedir_name_len);
	entry_name[basedir_name_len] = '/';

	dirp = opendir(basedir);
	vector_init(&statvec, sizeof(struct stat_data), free_stat_data);
	while ((entry = readdir(dirp))) {
		// Construct full entry name
		strcpy(entry_name + basedir_name_len + 1, entry->d_name);
		if (stat(entry_name, &statbuf) != 0) {
			perror("Failed to stat");
			goto cleanall;
		}
		make_stat_data(entry_name, &statbuf, &data);
		vector_push(&statvec, &data);
	}

	printf("%lu files listed\n", vector_len(&statvec));
	sort_files(&statvec);
	for (i = 0; i < vector_len(&statvec); i++) {
		const struct stat_data *data = vector_at_ref(&statvec, i);
		printf("Filename: %s\n", data->filename);
	}

cleanall:
	vector_free(&statvec);
	closedir(dirp);
	free(basedir);
	free(entry_name);
	return 0;
}

#include <gtk/gtk.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <stdio.h>
#include <stdlib.h>
#include "loader.h"

GLuint program_id;

static gboolean render(GtkGLArea *area, GdkGLContext *context)
{
	GLuint va_id, vertex_buffer;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(program_id);

	glGenVertexArrays(1, &va_id);
	glBindVertexArray(va_id);

	static const GLfloat vertex_data[] = {
		-1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
	};

	glGenBuffers(1, &vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data), vertex_data,
		     GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);

	glDrawArrays(GL_TRIANGLES, 0, 3);
	glDisableVertexAttribArray(0);
	return TRUE;
}

static void on_realize(GtkGLArea *area, gpointer data)
{
	GLuint vertex_shader, fragment_shader;

	gtk_gl_area_make_current(area);
	if (gtk_gl_area_get_error(area))
		return;
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to init GLEW\n");
		exit(1);
	}

	program_id = glCreateProgram();
	vertex_shader = load_shader_from_disk(
		SK_Vertex, "simple_vertex_shader.vert", program_id);
	fragment_shader = load_shader_from_disk(
		SK_Fragment, "simple_fragment_shader.frag", program_id);

	glLinkProgram(program_id);
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);
}

static void activate(GtkApplication *app, gpointer user_data)
{
	GtkWidget *window;
	GtkWidget *frame;
	GtkWidget *gl_area;

	window = gtk_application_window_new(app);
	gtk_window_set_title(GTK_WINDOW(window), "OpenGL Basics");
	gtk_window_set_default_size(GTK_WINDOW(window), 500, 500);

	frame = gtk_frame_new(NULL);
	gtk_window_set_child(GTK_WINDOW(window), frame);

	gl_area = gtk_gl_area_new();
	gtk_widget_set_size_request(frame, 400, 400);
	gtk_frame_set_child(GTK_FRAME(frame), gl_area);
	g_signal_connect(gl_area, "render", G_CALLBACK(render), NULL);
	g_signal_connect(gl_area, "realize", G_CALLBACK(on_realize), NULL);

	gtk_widget_show(window);
}

int main(int argc, char **argv)
{
	GtkApplication *app;
	int status;

	app = gtk_application_new("org.deep.gl", G_APPLICATION_FLAGS_NONE);
	g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
	status = g_application_run(G_APPLICATION(app), argc, argv);
	g_object_unref(app);

	return status;
}
